//preloader

$(window).on('load', function() { // makes sure the whole site is loaded
  $('#status').fadeOut(); // will first fade out the loading animation
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
  $('body').delay(350).css({'overflow':'visible'});
})

$('document').ready(function(){
  $('.content-slide').flexslider({
    animation: 'slide',
    // directionNav: true,
    controlNav: false,
    controlsContainer: $(".custom-controls-container"),
    customDirectionNav: $(".custom-navigation a")
  });

  $('section').hover(function(){
    $(this).find('.modal').toggleClass('m-show');
  });


  //drive to section
  $('a[href^="#"]').on("click", function(e) {
        var o = $(this.getAttribute("href"));
        o.length && (e.preventDefault(), $("html, body").stop().animate({
            scrollTop: o.offset().top
        }, 1e3))
    });

  //drive to section por divs
  $(document).on("click", function(e) {
        
    });


  // botones modales lo se, es un desastre pero la prisa...

  $('.m1 .m-trigger').click(function(){
    $('.m1 .content').addClass('m-show');
    $('body').addClass('lock-body');
  });
  $('.fa-close').click(function() {
    $('.m1 .content').removeClass('m-show');
    $('body').removeClass('lock-body');
  });

  $('.m2 .m-trigger').click(function(){
    $('.m2 .content').addClass('m-show');
    $('body').addClass('lock-body');
  });
  $('.fa-close').click(function() {
    $('.m2 .content').removeClass('m-show');
    $('body').removeClass('lock-body');
  });

  $('.m3 .m-trigger').click(function(){
    $('.m3 .content').addClass('m-show');
    $('body').addClass('lock-body');
  });
  $('.fa-close').click(function() {
    $('.m3 .content').removeClass('m-show');
    $('body').removeClass('lock-body');
  });

  $('.m4 .m-trigger').click(function(){
    $('.m4 .content').addClass('m-show');
    $('body').addClass('lock-body');
  });
  $('.fa-close').click(function() {
    $('.m4 .content').removeClass('m-show');
    $('body').removeClass('lock-body');
  });

  $('.m5 .m-trigger').click(function(){
    $('.m5 .content').addClass('m-show');
    $('body').addClass('lock-body');
  });
  $('.fa-close').click(function() {
    $('.m5 .content').removeClass('m-show');
    $('body').removeClass('lock-body');
  });

  $('.m6 .m-trigger').click(function(){
    $('.m6 .content').addClass('m-show');
    $('body').addClass('lock-body');
  });
  $('.fa-close').click(function() {
    $('.m6 .content').removeClass('m-show');
    $('body').removeClass('lock-body');
  });

  $('.m7 .m-trigger').click(function(){
    $('.m7 .content').addClass('m-show');
    $('body').addClass('lock-body');
  });
  $('.fa-close').click(function() {
    $('.m7 .content').removeClass('m-show');
    $('body').removeClass('lock-body');
  });

  $('.m8 .m-trigger').click(function(){
    $('.m8 .content').addClass('m-show');
    $('body').addClass('lock-body');
  });
  $('.fa-close').click(function() {
    $('.m8 .content').removeClass('m-show');
    $('body').removeClass('lock-body');
  });

  $('.m9 .m-trigger').click(function(){
    $('.m9 .content').addClass('m-show');
    $('body').addClass('lock-body');
  });
  $('.fa-close').click(function() {
    $('.m9 .content').removeClass('m-show');
    $('body').removeClass('lock-body');
  });

});

var $document = $(document);
$document.scroll(function(){
  if($document.scrollTop() >= 700){
  $('.go-down i').addClass('down');
}
else{
  $('.go-down i').removeClass('down');
}
});


////////////////////
////MAGIC SCROLL///
//////////////////

//INTRO
//estacion1
$(function(){
  var granos = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to("header .b1", 1, {top: '15%'}))
    .add(TweenMax.to("header .b2", 0.9, {top: '20%'}))
    .add(TweenMax.to("header .b3", 1.5, {top: '70%'}))

  new ScrollMagic.Scene({triggerElement: ".brand",offset: -400}).setTween(timeline).addTo(granos);
});

//estacion1
$(function(){
  var materos = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to(".materos .m1", 0.1, {left: '0%'}))
    .add(TweenMax.to(".materos .m2", 0.1, {left: '21%'}))
    .add(TweenMax.to(".materos .m3", 0.1, {left: '42%'}))
    .add(TweenMax.to(".materos .m4", 0.1, {left: '63%'}))
    .add(TweenMax.to(".materos .m5", 0.1, {left: '5%'}))
    .add(TweenMax.to(".materos .m6", 0.1, {left: '24%'}))
    .add(TweenMax.to(".materos .m7", 0.1, {left: '43%'}))
    .add(TweenMax.to(".materos .m8", 0.1, {left: '60%'}))
    .add(TweenMax.to(".materos .m9", 0.1, {left: '8%'}))
    .add(TweenMax.to(".materos .m10", 0.1, {left: '26%'}))
    .add(TweenMax.to(".materos .m11", 0.1, {left: '45%'}))
    .add(TweenMax.to(".materos .m12", 0.1, {left: '60%'}))
    .add(TweenMax.to(".materos .luz", 0.8, {opacity: '1'}))
  new ScrollMagic.Scene({triggerElement: ".materos",offset: -50}).setTween(timeline).addTo(materos);
});

// estacion2
$(function(){
  var bicho = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to(".planta-est2 .est1-rama1", 0.2, {right: '-25px'}))
    .add(TweenMax.to(".planta-est2 .est1-rama2", 0.2, {right: '-25px'}))
  new ScrollMagic.Scene({triggerElement: ".estacion2",offset: -250}).setTween(timeline).addTo(bicho);
});

// estacion3
$(function(){
  var agua = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to(".planta-est3 .planta1-est3", 0.2, {left: '0px'}))
    .add(TweenMax.to(".planta-est3 .agua", 1.8, {top: '25px', opacity: '1'}))
    .add(TweenMax.to(".planta2-est3", 2, {left: '200px'}))
  new ScrollMagic.Scene({triggerElement: ".estacion3",offset: 150}).setTween(timeline).addTo(agua);
});

// estacion4
$(function(){
  var cesta = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to(".planta-est4 .rama1", 1, {right: '-5px'}))
    .add(TweenMax.to(".planta-est4 .rama2", 0.8, {right: '-50.8px'}))
    .add(TweenMax.to(".cesta", 1.5, {right: '190px'}))
  new ScrollMagic.Scene({triggerElement: ".estacion4",offset: -200}).setTween(timeline).addTo(cesta);
});

// estacion5
$(function(){
  var cajita = new ScrollMagic.Controller();
  var tween = TweenMax.fromTo(".planta-est5 .c-semillas", 0.2, {left: -20}, {left: 20, repeat: -1, yoyo: true, ease: Circ.easeInOut});
  var scene = new ScrollMagic.Scene({triggerElement: ".estacion5", offset: -0, duration: 200}).setTween(tween).addTo(cajita);
});


// estacion6
$(function(){
  var cesta = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to(".planta-est6 .caja", 1, {right: '60px', transform: 'roatate(46deg)'}))
    .add(TweenMax.to(".semillas", 1.5, {right: '0px'}))
  new ScrollMagic.Scene({triggerElement: ".estacion6",offset: 200}).setTween(timeline).addTo(cesta);
});

$(function(){
  var rastrillo = new ScrollMagic.Controller();
  var tweenRastrillo = TweenMax.fromTo(".rastrillo", 0.8, {right: -20}, {right: 6, repeat: -1, yoyo: true, ease: Circ.easeInOut});
  var scene = new ScrollMagic.Scene({triggerElement: ".rastrillo", offset: -200, duration: 300}).setTween(tweenRastrillo).addTo(rastrillo);
});

// estacion7
$(function(){
  var cesta = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to(".planta-est7", 1.5, {left: '0px'}))
  new ScrollMagic.Scene({triggerElement: ".estacion7",offset: -100}).setTween(timeline).addTo(cesta);
});

// estacion8
$(function(){
  var sacos = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to(".planta-est8 .saco-back", 0.8, {left: '0px'}))
    .add(TweenMax.to(".planta-est8 .saco-front", 1.5, {left: '0px'}))
    .add(TweenMax.to(".planta-est8 .saco-flip", 1.5, {left: '580px', transform: 'rotate(60deg)'}))
    .add(TweenMax.to(".planta-est8 .semillas", 1.8, {top: '10px', opacity: '1'}))
    .add(TweenMax.to(".planta-est8 .semillas2", 1.8, {top: '360px', opacity: '1'}))
  new ScrollMagic.Scene({triggerElement: ".estacion8",offset: -100}).setTween(timeline).addTo(sacos);
});

// estacion9
$(function(){
  var maquina = new ScrollMagic.Controller();
  var tweenMaquina = TweenMax.fromTo(".maquina", 0.2, {transform: 'rotate(-4deg)'}, {transform: 'rotate(4deg)', repeat: -1, yoyo: true, ease: Circ.easeInOut});
  var tweenMaquina = TweenMax.fromTo(".s-small", 0.2, {transform: 'rotate(4deg)', opacity: '1'}, {transform: 'rotate(-4deg)', repeat: -1, yoyo: true, ease: Circ.easeInOut});
  var scene = new ScrollMagic.Scene({triggerElement: ".estacion9", offset: -150, duration: 300}).setTween(tweenMaquina).addTo(maquina);
});

//estacion10
$(function(){
  var sacos = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to(".planta-est10 .envoltorio", 0.8, {right: '0px'}))
    .add(TweenMax.to(".planta-est10 .choco", 1.2, {right: '500px'}))
  new ScrollMagic.Scene({triggerElement: ".estacion10",offset: -100}).setTween(timeline).addTo(sacos);
});


///arrow out
$(function(){
  var sacos = new ScrollMagic.Controller();
  var timeline = new TimelineMax()
    .add(TweenMax.to(".go-down i", 0.5, {opacity: '0px', display: 'none'}))
  new ScrollMagic.Scene({triggerElement: ".importancia",offset: -100}).setTween(timeline).addTo(sacos);
});

///recorrido////
//track1
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	// var $word = $("path#word");
  	var $trace1 = $("#_x31_ .trace1");
  	var $trace2 = $("#_x31_ .trace2");
  	var $copy = $("#_x32__ESTATICO .vector-copy");

  	// prepare SVG
  	// pathPrepare($word);
  	pathPrepare($trace1);
  	pathPrepare($trace2);
  	pathPrepare($copy);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tween = new TimelineMax()
  		.add(TweenMax.to($trace1, 2, {delay: 2.5, strokeDasharray: "900", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 2, {delay: 2.5, strokeDasharray: "900", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($copy, 4, {opacity:'1', delay: 2.5, ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".estacion1", duration: 600})
  					.setTween(tween)
  					// .addIndicators() // add indicators (requires plugin)
  					.addTo(controller);

});


//track2
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	// var $word = $("path#word");
  	var $trace1 = $("#_x33_ .trace1");
  	var $trace2 = $("#_x33_ .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenTwo = new TimelineMax()
  		// .add(TweenMax.to($word, 0.9, {strokeDashoffset: 0, ease:Linear.easeNone})) // draw word for 0.9
  		// .add(TweenMax.to($dot, 0.1, {strokeDashoffset: 0, ease:Linear.easeNone}))  // draw dot for 0.1
  		.add(TweenMax.to($trace1, 2, {strokeDasharray: "900", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 2, {strokeDasharray: "900", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({offset:"-450", triggerElement: ".estacion2", duration: 200, tweenChanges: true})
  					.setTween(tweenTwo)
  					// .addIndicators() // add indicators (requires plugin)
  					.addTo(controller);

});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	// var $word = $("path#word");
  	var $trace1 = $("#_x34_ .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenTwo = new TimelineMax()
  		// .add(TweenMax.to($word, 0.9, {strokeDashoffset: 0, ease:Linear.easeNone})) // draw word for 0.9
  		// .add(TweenMax.to($dot, 0.1, {strokeDashoffset: 0, ease:Linear.easeNone}))  // draw dot for 0.1
  		.add(TweenMax.to($trace1, 2, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".estacion2", offset: -400, duration: 200, tweenChanges: true})
  					.setTween(tweenTwo)
  					// .addIndicators() // add indicators (requires plugin)
  					.addTo(controller);

});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	// var $word = $("path#word");
  	var $trace1 = $("#_x35_ .trace1");
  	var $trace2 = $("#_x35_ .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenTwo = new TimelineMax()
  		// .add(TweenMax.to($word, 0.9, {strokeDashoffset: 0, ease:Linear.easeNone})) // draw word for 0.9
  		// .add(TweenMax.to($dot, 0.1, {strokeDashoffset: 0, ease:Linear.easeNone}))  // draw dot for 0.1
  		.add(TweenMax.to($trace1, 2, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 2, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x34_", offset: -200, duration: 200, tweenChanges: true})
  					.setTween(tweenTwo)
  					// .addIndicators() // add indicators (requires plugin)
  					.addTo(controller);

});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	// var $word = $("path#word");
  	var $trace1 = $("#_x36__ESTATICO .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenTwo = new TimelineMax()
  		// .add(TweenMax.to($word, 0.9, {strokeDashoffset: 0, ease:Linear.easeNone})) // draw word for 0.9
  		// .add(TweenMax.to($dot, 0.1, {strokeDashoffset: 0, ease:Linear.easeNone}))  // draw dot for 0.1
  		.add(TweenMax.to($trace1, 2, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x35_", offset: -400, duration: 200, tweenChanges: true})
  					.setTween(tweenTwo)
  					// .addIndicators() // add indicators (requires plugin)
  					.addTo(controller);

});

//trace3
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x37_ .trace3");
  	var $trace2 = $("#_x37_ .trace4");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 1.5, {strokeDasharray: "900", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 1, {strokeDasharray: "900", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x36__ESTATICO", offset: -200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x38_ .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".estacion3", offset: -200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x39_ .trace1");
  	var $trace2 = $("#_x39_ .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
      .add(TweenMax.to($trace1, 4, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
      .add(TweenMax.to($trace2, 6, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x38_", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x31_0_ESTATICO .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 1, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".estacion4", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});


//track4
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $text = $("#_x31_0_ESTATICO .trace");
  	var $trace1 = $("#_x31_1 .trace1");
  	var $trace2 = $("#_x31_1 .trace2");

  	// prepare SVG
  	pathPrepare($text);
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($text, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
      .add(TweenMax.to($trace1, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
      .add(TweenMax.to($trace2, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)

  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".estacion4", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $text = $("#_x31_2 .trace");
  	var $trace1 = $("#_x31_3 .trace1");
  	var $trace2 = $("#_x31_3 .trace2");

  	// prepare SVG
  	pathPrepare($text);
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($text, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
      .add(TweenMax.to($trace1, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
      .add(TweenMax.to($trace2, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)

  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x31_2", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

// $('document').ready(function(){
//   function pathPrepare ($el) {
//   		var lineLength = $el[0].getTotalLength();
//   		$el.css("stroke-dasharray", lineLength);
//   		$el.css("stroke-dashoffset", lineLength);
//   	}
//
//   	var $trace1 = $("#_x31_4_ESTATICO .trace");
//
//   	// prepare SVG
//   	pathPrepare($trace1);
//
//   	// init controller
//   	var controller = new ScrollMagic.Controller();
//
//   	// build tween
//   	var tweenThree = new TimelineMax()
//   		.add(TweenMax.to($trace1, 1, {opacity: "1", ease:Linear.easeNone}), 0)
//   	// build scene
//   	var scene = new ScrollMagic.Scene({triggerElement: "#_x31_4_ESTATICO", duration: 200, tweenChanges: true})
//   					.setTween(tweenThree)
//   					.addTo(controller);
// });


// track5
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $(".copy-holder .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 1, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({ triggerElement: ".estacion5", offset: -200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});



//track6
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x31_6 .trace1");
  	var $trace2 = $("#_x31_6 .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x31_6", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});


//track7
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x31_7_ESTATICO .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".estacion6", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x31_7_ESTATICO .trace");
  	var $trace2 = $("#_x31_7_ESTATICO .trace");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".planta-est6 .caja", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x31_8 .trace1");
  	var $trace2 = $("#_x31_8 .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenlineSeven = new TimelineMax()
  		.add(TweenMax.to($trace1, 1, {strokeDashoffset: "900", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 1.5, {strokeDashoffset: "900", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".estacion6", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenlineSeven)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x31_9_copy .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x31_7_ESTATICO", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x32_0 .trace1");
  	var $trace2 = $("#_x32_0 .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x31_9_copy", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});


//track8
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x32_2_copy .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenTextEight = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".planta-est7", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenTextEight)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x32_3 .trace1");
  	var $trace2 = $("#_x32_3 .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x32_2_copy", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x32_5_copy .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x32_4", offset:-200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

//track9
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x32_6 .trace1");
  	var $trace2 = $("#_x32_6 .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".planta-est8", duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x32_7_ESTATICO .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".estacion9", duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x32_8 .trace1");
  	var $trace2 = $("#_x32_8 .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x32_7_ESTATICO", duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

//track10
$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x32_9_copy .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x32_8", offset: 200, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x33_0 .trace1");
  	var $trace2 = $("#_x33_0 .trace2");

  	// prepare SVG
  	pathPrepare($trace1);
  	pathPrepare($trace2);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  		.add(TweenMax.to($trace2, 3, {strokeDashoffset: "0", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: "#_x32_9_copy", duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});

$('document').ready(function(){
  function pathPrepare ($el) {
  		var lineLength = $el[0].getTotalLength();
  		$el.css("stroke-dasharray", lineLength);
  		$el.css("stroke-dashoffset", lineLength);
  	}

  	var $trace1 = $("#_x33_1_ESTATICO .trace");

  	// prepare SVG
  	pathPrepare($trace1);

  	// init controller
  	var controller = new ScrollMagic.Controller();

  	// build tween
  	var tweenThree = new TimelineMax()
  		.add(TweenMax.to($trace1, 2.8, {opacity: "1", ease:Linear.easeNone}), 0)
  	// build scene
  	var scene = new ScrollMagic.Scene({triggerElement: ".choco", offset: -500, duration: 200, tweenChanges: true})
  					.setTween(tweenThree)
  					.addTo(controller);
});
